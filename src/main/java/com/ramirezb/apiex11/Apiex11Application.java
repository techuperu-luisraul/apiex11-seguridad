package com.ramirezb.apiex11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apiex11Application {

	public static void main(String[] args) {
		SpringApplication.run(Apiex11Application.class, args);
	}

}
